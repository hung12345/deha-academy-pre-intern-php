<?php
include_once "User.php";
$id = null;
$user = null;
if($_GET['id']){
    $id = $_GET['id'];
    $user = User::find($id);
}else{
    header("location:./index.php");
}
?>
<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Crud user show</title>
    <style>
        tbody, td, tfoot, th, thead, tr {
            border-color: inherit;
            border-style: solid;
            border-width: 0;
            width: 33.33333%;
        }
    </style>
</head>
<body>
<div class="container">
    <div>
        <h1>Show user list</h1>
    </div>
    <?php if($user){ ?>
        <p style="text-transform: uppercase; font-weight: bold; text-align: center">User information.</p>
        <table class="table" style="text-align: center">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Password</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?= $user['name'] ?></td>
                    <td><?= $user['email'] ?></td>
                    <td><?= $user['password'] ?></td>
                </tr>
            </tbody>
        </table>
        <a href="index.php" class="btn btn-primary">Back to list</a>

<!--    --><?php //}else{ ?>
<!--        <p>User Not Found.</p>-->
<!--        <a href="index.php" class="btn btn-primary">Back to list</a>-->
    <?php }?>

</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>
</html>
