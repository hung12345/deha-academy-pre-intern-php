<?php
include_once 'User.php';
if (isset($_POST['login']) && !empty($_POST['email']))
{

    $user = User::login($_POST['email'], $_POST['password']);
   if (empty($user)){
       $errorMessage= "Tai khoan hoac mat khau khong chinh xac!";
//       header('Location: ./login.php');
   }else{
       $_SESSION['message'] = "Đăng nhập thành công!";
       header('Location: ./index.php');
       die();
   }
}
?>
<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://kit.fontawesome.com/04d7898571.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Crud user</title>
    <style>
        body{
            font-size: 16px;
        }
        #wrapper{
            min-height: 100vh;
            /*display: flex;*/
            display: block;
            justify-content: center;
            align-items: center;
        }
        #form-login{
            max-width: 400px;
            background: rgba(0, 0, 0 , 0.8);
            flex-grow: 1;
            padding: 30px 30px 40px;
            box-shadow: 0px 0px 17px 2px rgba(255, 255, 255, 0.8);
            margin: 5% 32%;
        }
        .form-heading{
            font-size: 25px;
            color: #f5f5f5;
            text-align: center;
            margin-bottom: 30px;
        }
        .form-group{
            border-bottom: 1px solid #fff;
            margin-top: 15px;
            margin-bottom: 20px;
            display: flex;
        }
        .form-group i{
            color: #fff;
            font-size: 14px;
            padding-top: 5px;
            padding-right: 10px;
        }
        .form-input{
            background: transparent;
            border: 0;
            outline: 0;
            color: #f5f5f5;
            flex-grow: 1;
        }
        .form-input::placeholder{
            color: #f5f5f5;
        }
        #eye i{
            padding-right: 0;
            cursor: pointer;
        }

        .form-submit{
            background: transparent;
            border: 1px solid #f5f5f5;
            color: #fff;
            width: 48%;
            text-transform: uppercase;
            padding: 6px 10px;
            transition: 0.25s ease-in-out;
            margin-top: 30px;
        }
        .form-submit a{
            color: #fff;
            text-decoration: none;
        }
        .form-submit:hover{
            border: 1px solid #54a0ff;
        }
    </style>
</head>
<body>
<div id="wrapper">
    <?php if(isset($_SESSION['success_message'])){ ?>
        <div style="text-align: center" class="alert alert-success alert-dismissible fade show" id="success_message" role="alert">
            <p>
                <?php echo($_SESSION['success_message']); unset($_SESSION['success_message']);?>
            </p>

            <script>
                var Message = document.getElementById("success_message");
                Message.style.opacity = 1;
                setTimeout(function() {
                    var Fade = setInterval(function () {
                        if (!Message.style.opacity) {
                            Message.style.opacity = 1;
                        }
                        if (Message.style.opacity > 0) {
                            Message.style.opacity -= 0.1;
                        } else {
                            clearInterval(Fade);
                            Message.style.display = "none";
                        }
                    }, 100);
                },2000);
            </script>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <?php } ?>

<!--        <div>--><?php //if(isset($_SESSION['success_message'])) {
//            echo $_SESSION['success_message'];
//            unset($_SESSION['success_message']);
//        }?>
<!--        </div>-->

    <form action="" id="form-login" method="post">
        <h1 class="form-heading">Login</h1>
        <div class="form-group">
            <i class="far fa-user"></i>
            <input type="text" class="form-input" name="email" placeholder="Email" required>
        </div>
        <div class="form-group">
            <i class="fas fa-key"></i>
            <input type="password" class="form-input" name="password" placeholder="Password" required>
            <div id="eye">
                <i class="far fa-eye"></i>
            </div>
        </div>
        <?php
            if(!empty($errorMessage)){
                echo "<span style='color: red'>$errorMessage</span>";
            }
        ?>
        <div>
            <button class="form-submit" type="submit" name="login">Login</button>
            <button class="form-submit" style="float: right" name="register"><a href="register.php">Register</a></button>
<!--            <input type="submit" value="Login" class="form-submit">-->
<!--            <input style="float: right" type="submit" value="Register" class="form-submit">-->
        </div>
    </form>
</div>


<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script>
    $(document).ready(function(){
        $('#eye').click(function(){
            $(this).toggleClass('open');
            $(this).children('i').toggleClass('fa-eye-slash fa-eye');
            if($(this).hasClass('open')){
                $(this).prev().attr('type', 'text');
            }else{
                $(this).prev().attr('type', 'password');
            }
        });
    });
</script>
</body>
</html>
