<?php

include_once 'DB.php';

class User
{
    static public function all()
    {
        $sql = "select * from users";
        $users = DB::execute($sql);
        return $users;
    }

    static public function create($dataCreate)
    {
        $sql = "INSERT INTO users (name, email, password) values (:name, :email, :password)";
        DB::execute($sql, $dataCreate);
    }

    static public function find($id)
    {
        $sql = "Select * from users where id =:id";
        $dataFind = ['id' => $id];
        $user = DB::execute($sql, $dataFind);
        return count($user) > 0 ? $user[0] : [];
    }

    static public function update($dataUpdate)
    {
        $sql = "Update users set name=:name, email=:email, password=:password where id =:id";
        DB::execute($sql, $dataUpdate);
    }

    static public function delete($id)
    {
        $sql = "Delete from users where id =:id;";
        $dataDelete = ['id' => $id];
        DB::execute($sql, $dataDelete);
    }
    static public function login($email, $pasword)
    {
        $sql = "Select * from users where email=:email and password=:password";
        $dataLogin = ['email' => $email,'password'=>$pasword];
        $user = DB::execute($sql, $dataLogin);
        return $user[0] ?? [];
    }
    static public function register($email)
    {
        $sql = "Select * from users where email=:email ";
        $dataRegister = ['email' => $email];
        $user = DB::execute($sql, $dataRegister);
        return $user[0] ?? [];
    }
    static public function findName($name)
    {
        $sql = "Select * from users where name =:name";
        $datafindName = ['name'=> $name];
        $user = DB::execute($sql, $datafindName);
        return $user;
    }
    static public function count()
    {
        $sql = "Select count(*) from users";
        DB::execute($sql);
    }
    static public function paginate($offset,$limit)
    {
        $sql = "Select * from users LIMIT $offset,$limit ";
        $data= DB::execute($sql);
        return $data;
    }

}