<?php

include_once "User.php";

if(isset($_POST['id'])){
    $id = $_POST['id'];
    User::delete($id);
    $_SESSION['message'] = 'Delete success';
    header('location: ./index.php');
}else{
    $_SESSION['message'] = 'User not found';
    header('location: ./index.php');
}
