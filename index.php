<?php
include_once "User.php";
include_once "DB.php";
$users = User::all();

//$limit = 5;
//$total = ceil(count($users)/$limit);
//$offset = ($page-1)*$limit;
////$sql = "SELECT * FROM users limit $offset, $limit";
//$data = User::paginate($offset,$limit);
//$limit = 5;
//$total = ceil(count($users) / $limit);
//$page = isset($_GET['page']) ? $_GET['page'] : 1;
//$offset = ($page - 1) * $limit;
//
////$sql = "SELECT * FROM users LIMIT $offset, $limit";
//$data = User::paginate($offset, $limit);
//

//}
$limit = 5;
$total_records = count($users);
$total_pages = ceil($total_records / $limit);
$current_page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
$prev_page = $current_page - 1;
$next_page = $current_page + 1;

$offset = ($current_page - 1) * $limit;
//$sql = "SELECT * FROM users LIMIT $offset, $limit";
$data = User::paginate($offset, $limit);


//Tìm kiếm theo tên
if (isset($_POST['search'])) {
//    header("location:./findname.php");
    $name = isset($_POST['namesearch']) ? trim($_POST['namesearch']) : '';
    $data = empty($name) ? [] : User::findName($name);
}
//Phân trang


?>
<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Crud user</title>
    <style>
        .action{
            display: flex;
        }
    </style>
</head>
<body>
<div class="container">
    <div >
        <div style="display: block">
            <h1 style="display: inline-block">User list</h1>
            <a href="login.php" style="float: right !important;">Log out</a>
        </div>

    </div>

    <?php if(isset($_SESSION['message'])){ ?>
        <div class="alert alert-success alert-dismissible fade show" id="message" role="alert">
            <p>
                <?php echo($_SESSION['message']); unset($_SESSION['message']);?>
            </p>

                <script>
                    var Message = document.getElementById("message");
                        Message.style.opacity = 1;
                    setTimeout(function() {
                        var Fade = setInterval(function () {
                        if (!Message.style.opacity) {
                            Message.style.opacity = 1;
                        }
                        if (Message.style.opacity > 0) {
                            Message.style.opacity -= 0.1;
                        } else {
                            clearInterval(Fade);
                            Message.style.display = "none";
                        }
                        }, 100);
                    },2000);
                </script>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    <?php } ?>

    <form action="" method="post">
    <div style="display: flex">
        <a href="create.php" class="btn btn-primary">Create</a>&nbsp&nbsp
        <input name="namesearch" style="margin: 4px 0px" type="text" placeholder="Name">
        <button type="submit" name="search" style="margin: 4px 0px">Search</button>
    </div>
    </form>



    <div>
        <?php if(count($data) > 0 || count($users)>0) { ?>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
<!--                <th scope="col">Password</th>-->
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data as $user) { ?>
            <tr>
                <th scope="row"><?= $user['id'] ?></th>
                <td><?= $user['name'] ?></td>
                <td><?= $user['email'] ?></td>
<!--                <td>--><?php //= $user['password'] ?><!--</td>-->

                <td class="action">
                    <a href="show.php?id=<?=$user['id']?>" class="btn btn-info">Show</a>
                    <a href="edit.php?id=<?=$user['id']?>" class="btn btn-warning">Edit</a>
                    <form action="delete.php" method="post" id="formDelete-<?= $user['id']?>">
                        <input type="hidden" name="id" value="<?= $user['id']?>">
                    <button type="button" class="btn btn-danger btn-delete" id="<?= $user['id']?>">Delete</button>
                    </form>
                </td>
            </tr>
            <?php } ?>
            </tbody>
        </table>

        <?php }else{ ?>
            <h2>No Data.</h2>
        <?php } ?>
    </div>
<!--    <nav aria-label="Page navigation example">-->
<!--        <ul class="pagination justify-content-center">-->
<!--            --><?php //if ($current_page == 1) { ?>
<!--            <li class="page-item disabled">-->
<!--                <a class="page-link" href="" tabindex="-1" aria-disabled="true">Previous</a>-->
<!--            </li>-->
<!--            --><?php //} else {?>
<!--            <li class="page-item"><a class="page-link" href="?page=" . $prev_page . >Previous</a></li>';-->
<!--            --><?php //}?>
<!--            <li class="page-item"><a class="page-link" href="">1</a></li>-->
<!--            <li class="page-item"><a class="page-link" href="">2</a></li>-->
<!--            <li class="page-item"><a class="page-link" href="">3</a></li>-->
<!--            <li class="page-item">-->
<!--                <a class="page-link" href="">Next</a>-->
<!--            </li>-->
<!--        </ul>-->
<!--    </nav>-->
    <?php
        echo '<nav aria-label="Page navigation example">';
        echo '<ul class="pagination justify-content-center">';

            if ($current_page == 1) {
            echo '<li class="page-item disabled"><a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a></li>';
            } else {
            echo '<li class="page-item"><a class="page-link" href="?page=' . $prev_page . '">Previous</a></li>';
            }

            for ($i = 1; $i <= $total_pages; $i++) {
            if ($i == $current_page) {
            echo '<li class="page-item active"><a class="page-link" href="#">' . $i . '</a></li>';
            } else {
            echo '<li class="page-item"><a class="page-link" href="?page=' . $i . '">' . $i . '</a></li>';
            }
            }

            if ($current_page == $total_pages) {
            echo '<li class="page-item disabled"><a class="page-link" href="#" tabindex="-1" aria-disabled="true">Next</a></li>';
            } else {
            echo '<li class="page-item"><a class="page-link" href="?page=' . $next_page . '">Next</a></li>';
            }

            echo '</ul>';
        echo '</nav>';

    // Hiển thị kết quả
    foreach ($data as $item) {
    // Hiển thị mỗi bản ghi ở đây
    }
?>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script>
    let deleteBtns = document.querySelectorAll('.btn-delete');
    deleteBtns.forEach(function (item){
        item.addEventListener('click',function (event){
            if(confirm("Delete user")){
                let id = this.getAttribute('id');
                document.querySelector('#formDelete-' + id).submit();
            }

        })
    })
</script>
</body>
</html>