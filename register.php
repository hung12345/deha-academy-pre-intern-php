<?php


include_once "User.php";
include_once "helper.php";

//$errors = [];

if (isset($_POST['register'])) {


        $user = User::register($_POST['email']);
        if (!empty($user)){
            $errorMessage= "Tài khoản đã tồn tại!";
//       header('Location: ./login.php');
        }else{
            $_SESSION['success_message'] = "Đăng ký thành công!";
            $dataCreate = [
                'name' => $_POST['name'],
                'email' => $_POST['email'],
                'password' => ($_POST['password']),
            ];
            $userCreate = User::create($dataCreate);
            header('Location: ./login.php');
            die();
        }

}
//    $errors = validate($_POST, ['name', 'email', 'password']);
//    if(count($errors) <= 0){
//        $dataRegister = [
//            'name' => $_POST['name'],
//            'email' => $_POST['email'],
//            'password' => md5($_POST['password']),
//        ];
//        $userCreate = User::create($dataRegister);
//        $_SESSION['message'] = "Register Successfully!";
//        $errors = [];
//        header("location: ./login.php");
//    }
//}else{
//    $errors = [];
//}

?>
<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Create User</title>
</head>
<body>
<div class="container">
    <div>
        <h1>Register</h1>
    </div>
    <div>
        <form method="post">
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Email:</label>
                <input type="email" name="email" class="form-control" placeholder="Example@gmail.com"
                       id="exampleInputEmail1" aria-describedby="emailHelp" required>
                <div id="emailHelp" class="text-danger">
                    <?php echo isset($errors['email']) ? $errors['email'] : "" ?>
                </div>
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Name:</label>
                <input type="text" name="name" placeholder="Nguyen Van A" required class="form-control">
                <div id="emailHelp" class="text-danger">
                    <?php echo isset($errors['name']) ? $errors['name'] : "" ?>
                </div>
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Password:</label>
                <input type="password" name="password" placeholder="********" required class="form-control"
                       id="exampleInputPassword1">
                <div id="emailHelp" class="text-danger">
                    <?php echo isset($errors['password']) ? $errors['password'] : "" ?>
                </div>
            </div>
            <div>
                <?php
                if(!empty($errorMessage)){
                    echo "<span style='color: red'>$errorMessage</span>";
                }
                ?>
            </div>

            <button type="submit" name="register" class="btn btn-primary">Register</button>
        </form>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>

</body>
</html>